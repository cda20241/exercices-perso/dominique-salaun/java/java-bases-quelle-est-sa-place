import java.util.Scanner;


public class Main {
    /**
     * Cette classe permet à l'utilisateur de saisir une phrase et un caractère,
     * puis elle recherche et affiche la position de ce caractère dans la phrase.
     * Si le caractère est trouvé dans la phrase, elle affiche sa position en utilisant un adjectif numéral ordinal.
     * Si le caractère n'est pas trouvé, un message approprié est affiché.
     */


    public static void main(String[] args) {
        /**
         * La méthode principale du programme.
         *
         * @param args Les arguments de la ligne de commande (non utilisés dans ce programme).
         */
        Scanner scanner = new Scanner(System.in);

        // Étape 1 : Saisissez une phrase et un caractère
        System.out.print("Entrez une phrase : ");
        String phrase = scanner.nextLine();
        System.out.print("Entrez un caractère : ");
        char caractere = scanner.next().charAt(0);

        boolean caractereTrouve = false;

        // Parcourez la phrase pour trouver le caractère
        for (int i = 0; i < phrase.length(); i++) {
            if (phrase.charAt(i) == caractere) {
                caractereTrouve = true;
                System.out.println("On retrouve le caractère " + caractere + " à la position " + getOrdinal(i + 1));
            }
        }

        // Si le caractère n'est pas trouvé, affichez un message approprié
        if (!caractereTrouve) {
            System.out.println("Le caractère " + caractere + " n'est pas présent dans la phrase.");
        }
    }


    public static String getOrdinal(int number) {
        /**
         * Cette méthode prend un nombre entier positif et renvoie sa forme d'adjectif numéral ordinal.
         *
         * @param number Le nombre pour lequel on veut obtenir l'adjectif numéral ordinal.
         * @return L'adjectif numéral ordinal correspondant au nombre (par exemple, "1er" ou "2ème").
         * @throws IllegalArgumentException Si le nombre n'est pas un entier naturel strictement positif.
         */
        if (number < 1) {
            throw new IllegalArgumentException("Le nombre doit être un entier naturel strictement positif.");
        }

        if (number == 1) {
            return number + "er";
        } else {
            return number + "ème";
        }
    }
}
